'use strict';

const path = require('path');
const express = require('express');

const app = express();

app.get('*', function(req, res) {
  console.log('req.subdomains', req.subdomains);
  //res.send('Hello World!');
  if(req.subdomains && req.subdomains.length > 0) {
    const subdomain = req.subdomains[0];
    let filePath = req.path;
    if(filePath === '/') {
      filePath = 'index.html';
    }
    const resultantPath = path.join(subdomain, filePath);
    console.log('resultantPath', resultantPath);
    res.sendFile(resultantPath, { root: './www' });
  }
});


app.listen(5000, function() {
  console.log('Example app listening on port 5000!');
});
