# static-review-app-server

Serves files from directory according to the sub-domain.

I used this to setup [GitLab review apps](https://about.gitlab.com/2016/11/22/introducing-review-apps/) for a [simple static site project (`.gitlab-ci.yml`)](https://gitlab.com/madlittlemods/review-apps-nginx)

```
npm run start
```

 - `http://foo.localhost.dev/` -> `./www/foo/index.html`
 - `http://foo.localhost.dev/images/something.png` -> `./www/foo/images/something.png`
 - `http://staging.localhost.dev/` -> `./www/staging/index.html`
 - etc



# Setup localhost wildcard subdomain DNS

## macOS, `dnsmasq.conf`

 1. `brew install dnsmasq`
 1. *just following commands outputted from brew after the install*,
    1. `cp /usr/local/opt/dnsmasq/dnsmasq.conf.example /usr/local/etc/dnsmasq.conf`
    1. `sudo brew services start dnsmasq`
 1. In `/usr/local/etc/dnsmasq.conf`, append `address=/.localhost.dev/127.0.0.1`
 1. Run `sudo brew services restart dnsmasq` to refresh the config
 1. Configure your DNS
    1. `sudo mkdir -p /etc/resolver`
    1. .
        ```
        sudo tee /etc/resolver/dev >/dev/null <<EOF
        nameserver 127.0.0.1
        EOF
        ```
 1. Just to get the root domain also routing to localhost, in `/private/etc/hosts`, append `127.0.0.1 localhost.dev`
 1. Now requests to `foo.localhost.dev`, etc will router to `127.0.0.1`

See https://passingcuriosity.com/2013/dnsmasq-dev-osx/



## Windows

**Note:** In my case, I want access the server setup on a macbook(`192.168.1.135`) from a Windows machine but you can replace with `127.0.0.1` if running the server on Windows.

 1. Install [Acrylic DNS Proxy](http://mayakron.altervista.org/wikibase/show.php?id=AcrylicHome) (SourceForge :()
 1. In `C:\Program Files (x86)\Acrylic DNS Proxy\AcrylicHosts.txt`, add `192.168.1.135 *.localhost.dev`
 1. Start Acryllic
    1. It will add `Start Acryllic Service` and `Stop Acryllic Service` start menu entries but I prefer to use the console version "Run Acryllic Console Version"
    1. For reference, by default it intalls here `C:\Program Files (x86)\Acrylic DNS Proxy`
 1. Configure your DNS to use Acrylic
    1. Start -> Control Panel -> Network and Internet -> Network and Sharing Center -> Change Adapter Settings -> Right-click on the adapter, Properties -> TCP/IPv4
    1. Set "Use the following DNS server address": Preferred DNS Server: `127.0.0.1`
 1. Just to get the root domain also routing to localhost, in `C:\Windows\System32\drivers\etc\hosts`, append `192.168.1.135 localhost.dev`
 1. Now requests to `foo.localhost.dev`, etc will router to `192.168.1.135`

 See http://stackoverflow.com/a/9695861/796832
